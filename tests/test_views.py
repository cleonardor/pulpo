import os
import pytest
import tempfile
from datetime import datetime
from unittest.mock import patch, MagicMock

from app import app
from app.models import User


def get_url_from_response(response):
    return response.headers[2][1]


@patch('app.views.datetime')
def test_login_user_is_authenticate_and_good_token(mock_datetime):
    mock_datetime.now.return_value = datetime(2020, 2, 2)
    user = User(username='test', token='token', token_expiration=datetime(2020, 2, 4))
    with patch('app.views.current_user', user):
        # it should redirect to /index page
        response = app.test_client().get('/login')
    assert response.status_code == 302  # FOUND redirect to index
    assert 'index' in get_url_from_response(response)


@patch('app.views.datetime')
def test_login_user_is_authenticate_old_token(mock_datetime):
    mock_datetime.now.return_value = datetime(2020, 2, 4)
    user = User(username='test', token='token', token_expiration=datetime(2020, 2, 2))
    with patch('app.views.current_user', user):
        # it should show the login form
        response = app.test_client().get('/login')
    assert response.status_code == 200  # OK render template


@patch('app.views.LoginForm')
@patch('app.views.datetime')
def test_login_data_is_not_valid(mock_datetime, mock_loginform):
    mock_datetime.now.return_value = datetime(2020, 2, 4)
    loginform = mock_loginform.return_value
    loginform.validate_on_submit.return_value = False
    user = User(username='test', token='token', token_expiration=datetime(2020, 2, 2))
    with patch('app.views.current_user', user):
        # it should show the login form again because form is not valid
        response = app.test_client().post('/login')
    assert response.status_code == 200  # OK render template


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(kwargs['data'], kwargs['status_code'])


@patch('app.views.requests.get')
@patch('app.views.LoginForm')
@patch('app.views.datetime')
def test_login_data_is_valid_not_service_available(mock_datetime, mock_loginform, mock_requests):
    mock_datetime.now.return_value = datetime(2020, 2, 4)
    loginform = mock_loginform.return_value
    loginform.validate_on_submit.return_value = True
    mock_requests.return_value = mocked_requests_get(data={}, status_code=404)
    user = User(username='test', token='token', token_expiration=datetime(2020, 2, 2))
    with patch('app.views.current_user', user):
        response = app.test_client().post('/login', data={})
    assert response.status_code == 200  # OK render template
