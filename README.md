# Pulpo Warehouse Syncronization

This project allows Pulpo users initiate a session and sincronize their warehouses from pulpo System. For login and syncronization it uses Pulpo [API](https://show.pulpo.co/api/v1/swagger/index.html).

## Installation
This app could be clone from [gitlab](https://gitlab.com/) using this [repository](https://gitlab.com/cleonardor/pulpo)

### Cloning repository using ssh
```bash
$ git clone git@gitlab.com:cleonardor/pulpo.git
```

### Cloning repository using https
```bash
$ git clone https://gitlab.com/cleonardor/pulpo.git
```

## Usage
This app is not ready for production environment yet, it's already in development stage.

## Develop Environment
You can use [docker-compose](https://docs.docker.com/compose/) to build a develop environment to run the app or run tests and linter.

### Run app
```bash
$ docker-compose up -d
```
Visit [http://localhost:5000](http://localhost:5000) to see the app running.
### Run tests and linter
```bash
$ docker-compose build # if image doesn't exist
$ docker-compose run app sh ./local_build.sh
```
Output will show the linter and tests result. 
Also you can visit htmlcov/index.html in project folder to see the test coverage.