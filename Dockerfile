FROM python:alpine3.10

# Create the working directory (and set it as the working directory)
RUN mkdir -p /app
WORKDIR /app/

# update dev libraries to install psycopg2
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

# update pip
RUN pip install --upgrade pip

# Download Python dependencies
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt