"""views module"""
# pylint: disable=no-member,cyclic-import
from collections import defaultdict
from datetime import (
    datetime,
    timedelta,
)
import json
import requests
from flask import (
    render_template,
    flash,
    redirect,
    url_for,
    request,
)
from flask_login import (
    current_user,
    login_user,
    logout_user,
    login_required,
)
from werkzeug.urls import url_parse

from app import app, db
from app.forms import LoginForm
from app.models import (
    User,
    Warehouse,
    State,
)
from app.config import BASE_API_URL


@app.route('/')
@app.route('/index')
def index():
    """view for home page
    """
    warehouses = []
    state = None
    if current_user.is_authenticated:
        warehouses = Warehouse.query.filter_by(user_id=current_user.id).all()
        state = (
            State
            .query
            .filter_by(user_id=current_user.id)
            .order_by(State.datetime.desc())
            .first()
        )
    return render_template('index.html', title='Home', warehouses=warehouses, state=state)

# -------------------------------------------------------------------------------------------------

@app.route('/login', methods=['GET', 'POST'])
def login():
    """view for login page
    """
    # pylint: disable=too-many-branches
    if current_user.is_authenticated and current_user.token_expiration > datetime.now():
        return redirect(url_for('index'))

    page = None
    form = LoginForm()
    if form.validate_on_submit():
        service_response = requests.get(BASE_API_URL)
        if service_response.status_code == requests.codes.ok:
            username = form.username.data
            password = form.password.data
            user = User.query.filter_by(username=username).first()
            if user:
                if user.token_expiration > datetime.now(): # token still works
                    if user.check_password(password):
                        login_user(user)
                        page = redirect_next_page(request)
                    else:
                        flash('Invalid password, try it again!')
                else: # we need to update the token
                    auth_response = get_token(username, password)
                    if auth_response.status_code == requests.codes.unauthorized:
                        # we know the user exist (I hope nobody delete it)
                        flash('Invalid password, try it again!')
                    else:
                        update_or_create_user(auth_response, username, password, user)
                        app.logger.info("User updated: username %s", username)
                        page = redirect_next_page(request)
            else:  # user doesn't exist locally
                auth_response = get_token(username, password)
                if auth_response.status_code == requests.codes.unauthorized:
                    flash('Invalid username or password, try it again!')
                else:
                    update_or_create_user(auth_response, username, password)
                    app.logger.info("New user created: username %s", username)
                    page = redirect_next_page(request)
        else:
            flash('Service temporarily unavailable. Try again in few minutes.')
            app.logger.warning("Service unavailable")

    if not page:
        page = render_template('login.html', title='Sing In', form=form)
    return page

def get_token(username, password):
    """get a user token from pulpo api
    """
    payload = {
        "grant_type": "password",
        "username": username,
        "password": password,
        "scope": "default"
    }
    headers = {'content-type': 'application/json'}
    url = "{}/auth".format(BASE_API_URL)
    auth_response = requests.post(url, data=json.dumps(payload), headers=headers)
    return auth_response

def update_or_create_user(auth_response, username, password, user=None):
    """update or create a user from local database
    """
    if not user:
        user = User(username=username)
    auth_info = auth_response.json()
    token = auth_info['access_token']
    token_expiration = datetime.now() + timedelta(seconds=auth_info['expires_in'])
    user.token = token
    user.token_expiration = token_expiration
    user.set_password(password)
    db.session.add(user)
    db.session.commit()
    login_user(user)

def redirect_next_page(request_):
    """create a redirect to next page if exist or home page
    """
    # redirect to next page if exist
    next_page = request_.args.get('next')
    # avoid redirect to external page
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('index')
    return redirect(next_page)

# -------------------------------------------------------------------------------------------------

@app.route('/logout')
def logout():
    """view to logout user from system
    """
    logout_user()
    return redirect(url_for('index'))

# -------------------------------------------------------------------------------------------------

@app.route('/syncronize')
@login_required
def sync_warehouses():
    """view to syncronized user's warehouses from pulpo
    """
    local_warehouses_set = set(Warehouse.query.filter_by(user_id=current_user.id).all())
    local_warehouses_map = {}
    for warehouse in local_warehouses_set:
        local_warehouses_map[warehouse.id] = warehouse
    success, response = get_remote_warehouses()
    if not success:
        return response
    remote_warehouses_set = set(response)

    count = defaultdict(int)

    # if warehouse is in local and remote we need to update they
    for warehouse in remote_warehouses_set.intersection(local_warehouses_set):
        warehouse_update = local_warehouses_map[warehouse.id]
        warehouse_update.update(warehouse)
        db.session.add(warehouse_update)
        count['updated'] += 1

    # if warehouse in remote but not in local we need to add they
    for warehouse in remote_warehouses_set.difference(local_warehouses_set):
        warehouse.user_id = current_user.id
        db.session.add(warehouse)
        count['added'] += 1

    # if warehouse in local but not in remote we need to delete it
    for warehouse in local_warehouses_set.difference(remote_warehouses_set):
        db.session.delete(warehouse)
        count['deleted'] += 1

    state = State(
        datetime=datetime.now(),
        status='Success',
        added=count['added'],
        updated=count['updated'],
        deleted=count['deleted'],
        user_id=current_user.id
    )
    db.session.add(state)

    try:
        db.session.commit()
        app.logger.info(
            "Succesful sync! Added: %s Updated: %s Deleted: %s",
            count['added'],
            count['updated'],
            count['deleted']
        )
    except Exception as exc:  # pylint: disable=broad-except
        db.session.rollback()
        app.logger.warning("Sync failed")
        app.logger.exception(exc)
        state = State(
            datetime=datetime.now(),
            status='Failure',
            user_id=current_user.id
        )
        db.session.add(state)
        db.session.commit()

    return redirect(url_for('index'))

def get_remote_warehouses():
    """get warehouses from logged user from pulpo
    """
    headers = {
        'content-type': 'application/json',
        'Authorization': 'Bearer {}'.format(current_user.token)
    }
    url = "{}/warehouses".format(BASE_API_URL)
    warehouses_response = requests.get(url, headers=headers)
    response_ = None
    if warehouses_response.status_code == requests.codes.ok:
        warehouses = []
        for warehouse in warehouses_response.json()['warehouses']:
            warehouses.append(Warehouse(**warehouse))
        response_ = (True, warehouses)
    elif warehouses_response.status_code == requests.codes.unauthorized:
        # the token supposedly lasts 12 hours
        current_user.token_expiration = datetime.now()
        db.session.add(current_user)
        db.session.commit()
        logout_user()
        flash("Please login again to syncronize the warehouses.")
        response_ = (False, redirect(url_for('login', next=url_for('index'))))
    else:
        flash("Something was wrong, pleaes try it again in few minutes.")
        response_ = (False, redirect(url_for('index')))
    return response_
