"""models module"""
# pylint: disable=no-member,too-few-public-methods
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

from app import (
    db,
    login,
)


class User(UserMixin, db.Model):
    """User model
    represent a system logged user
    """
    # pylint: disable=missing-function-docstring
    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=redefined-builtin
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    warehouses = db.relationship('Warehouse', backref='user', lazy=True)
    states = db.relationship('State', backref='user', lazy=True)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


@login.user_loader
def load_user(id):
    """method required by flask_login. Let load the current logged user
    """
    #pylint: disable=invalid-name,redefined-builtin
    return User.query.get(int(id))


class Warehouse(db.Model):
    """Warehouse model
    Represent the warehouses associated to users
    """
    # pylint: disable=too-many-instance-attributes,redefined-builtin,invalid-name
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean)
    city = db.Column(db.String(128))
    country = db.Column(db.String(128))
    fax = db.Column(db.String(128))
    line1 = db.Column(db.String(128))
    line2 = db.Column(db.String(128))
    name = db.Column(db.String(128))
    phone = db.Column(db.String(128))
    priority = db.Column(db.Integer)
    site = db.Column(db.String(128))
    state = db.Column(db.String(128))
    tenant_id = db.Column(db.Integer)
    zip_code = db.Column(db.String(128))
    inserted_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, default=datetime.now(), onupdate=datetime.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)

    def update(self, other):
        """Let update a warehouse obje from information of other warehouse object
        """
        self.active = other.active
        self.city = other.city
        self.country = other.country
        self.fax = other.fax
        self.line1 = other.line1
        self.line2 = other.line2
        self.name = other.name
        self.phone = other.phone
        self.priority = other.priority
        self.site = other.site
        self.state = other.state
        self.zip_code = other.zip_code
        self.updated_at = datetime.now()

    def _extensive_repr(self):
        return "{id} {tenant_id}".format(
            id=self.id,
            tenant_id=self.tenant_id
        )

    def __repr__(self):
        return '<Warehouse {}>'.format(self.name)

    def __eq__(self, other):
        return self._extensive_repr() == other._extensive_repr()

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self._extensive_repr())


class State(db.Model):
    """State model
    Represent the status after warehouses syncronization
    """
    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=redefined-builtin
    datetime = db.Column(db.DateTime)
    status = db.Column(db.String(20))
    added = db.Column(db.Integer, default=0)
    updated = db.Column(db.Integer, default=0)
    deleted = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        message = '<State - Added: {} Updated: {} Deleted: {}>'
        return message.format(self.added, self.updated, self.deleted)
