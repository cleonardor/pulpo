"""app module"""
# pylint: disable=cyclic-import
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

from app.config import cfg


app = Flask(__name__)
app.config.from_object(cfg)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'

# avoid circular dependencies
from app import views, models  # pylint: disable=wrong-import-position

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
