"""config module"""
# pylint: disable=too-few-public-methods
from os import environ, urandom


class Config:
    """Config Class.
    Provide base configuration for all environments
    """
    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = environ.get("APP_CSRF_SESSION_KEY", default=urandom(16))

    # Secret key for signing cookies
    SECRET_KEY = environ.get("APP_SECRET_KEY", default=urandom(16))

    # sql alchemist options
    SQLALCHEMY_DATABASE_URI = "postgresql://{username}:{password}@{server}:{port}/{db}".format(
        username=environ.get("DB_USER"),
        password=environ.get("DB_PASSWORD"),
        server=environ.get("DB_HOST"),
        port=environ.get("DB_PORT"),
        db=environ.get("DB_NAME")
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    """Provide configuration for development environment
    """
    DEBUG = True
    TESTING = True
    SECRET_KEY = 'INSECURE_FOR_LOCAL_DEVELOPMENT'


class ProductionConfig(Config):
    """Provide configuration for production environment
    """
    DEBUG = False
    TESTING = False


environment = environ.get("FLASK_ENV", default="development")
if environment == "development":
    cfg = DevelopmentConfig()
elif environment == "production":
    cfg = ProductionConfig()
else:
    cfg = None  # pylint: disable=invalid-name

BASE_API_URL = environ.get("BASE_API_URL", default="localhost")
